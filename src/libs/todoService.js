const baseURL = 'http://localhost:8080/todos'

export const loadTodos = () => {
  return fetch(baseURL)
    .then(response => response.json())
}

export const createTodo = (todo) => {
  return fetch(baseURL, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(todo)
  }).then(response => response.json())
}

export const updateTodoService = (todo) => {
  return fetch(`${baseURL}/${todo.id}`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(todo)
  }).then(response => response.json())
}

export const deleteTodo = (id) => {
  return fetch(`${baseURL}/${id}`, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
}
