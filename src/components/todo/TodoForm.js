import React from 'react'

export const TodoForm = (props) => (
  <form onSubmit={props.handleSubmit}>
    <input type="text"
        onChange={props.handleInputValue}
        value={props.currentTodo}/>
  </form>
)

TodoForm.propTypes = {
  currentTodo: React.PropTypes.string.isRequired,
  handleInputValue: React.PropTypes.func.isRequired,
  handleSubmit: React.PropTypes.func.isRequired
}
