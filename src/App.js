import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import {TodoForm, TodoList, Footer} from './components/todo'
import {addTodo, generateId, findById, toggleTodo, updateTodo, removeTodo, filterTodos} from './libs/todoHelpers'
import {pipe, partial} from './libs/utils'
import {loadTodos, createTodo, updateTodoService, deleteTodo} from './libs/todoService'

class App extends Component {

  state = {
    todos : [],
    currentTodo: ''
  }

  static contextTypes = {
    route: React.PropTypes.string
  }

  componentDidMount() {
    loadTodos()
      .then(todos => this.setState({todos}))
  }

  handleRemove = (id, event) => {
    event.preventDefault()
    const updatedTodos = removeTodo(this.state.todos, id)
    this.setState({todos: updatedTodos})
    deleteTodo(id)
      .then(() => this.showTemporaryMessage('Todo removed'))
  }

  handleToggle = (id) => {
    const getToggledTodo = pipe(findById, toggleTodo)
    const updated = getToggledTodo(id, this.state.todos)
    const getupdatedTodos = partial(updateTodo, this.state.todos)
    const updatedTodos = getupdatedTodos(updated)

    this.setState({todos: updatedTodos})

    console.log(updated);

    updateTodoService(updated)
      .then(() => this.showTemporaryMessage('Todo updated'))
  }

  handleSubmit = (event) => {
    event.preventDefault()

    const newId = generateId()
    const newTodo = {id: newId, name: this.state.currentTodo, isComplete: false}
    const updatedTodos = addTodo(this.state.todos, newTodo)

    this.setState({
      todos: updatedTodos,
      currentTodo: '',
      errorMessage: ''
    })

    createTodo(newTodo)
      .then(() => this.showTemporaryMessage('Todo added'))
  }

  showTemporaryMessage = (msg) => {
    this.setState({message: msg})
    setTimeout(() => this.setState({message: ''}), 2500)
  }

  handleEmptySubmit = (event) => {
    event.preventDefault()

    this.setState({
      errorMessage: 'Please enter a todo name'
    })
  }

  handleInputValue = (event) => {
    this.setState({
      currentTodo: event.currentTarget.value
    })
  }

  render() {
    const submitHandler = this.state.currentTodo ? this.handleSubmit : this.handleEmptySubmit
    const displayTodos = filterTodos(this.state.todos, this.context.route)

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React todos</h2>
        </div>
        <div className="Todo-App">
          {this.state.errorMessage && <span className="error-message">{this.state.errorMessage}</span>}
          {this.state.message && <span className="success-message">{this.state.message}</span>}
          <TodoForm
            handleInputValue={this.handleInputValue}
            handleSubmit={submitHandler}
            currentTodo={this.state.currentTodo}/>

          <TodoList handleToggle={this.handleToggle}
            todos={displayTodos}
            handleRemove={this.handleRemove}/>

          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
